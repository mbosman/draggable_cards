from flask import Flask, request, render_template, jsonify
import json
app = Flask(__name__)

globallist = [
    {"name": "Mitchell", "skill": ["Medical", "two", "three"]},
    {"name": "Patrick", "skill": ["Medical", "Air", "zix"]},
    {"name": "Ilona", "skill": ["Test", "eight", "nine"]},
]
audit = [
    {"id": "0001", "Category": ['Air', 'Test', 'Medical', 'Test', 'Test'], "Auditors": ""},
    {"id": "0002", "Category": ['Test', 'Medical', 'Test', 'Test', 'Test'], "Auditors": ""},
    {"id": "0003", "Category": ['Test', 'Test', 'Test', 'Air', 'Test'], "Auditors": ""},
    {"id": "0004", "Category": ['Test', 'Air', 'Test', 'Test', 'Test'], "Auditors": ""},
    {"id": "0005", "Category": ['Medical', 'Test', 'Air', 'Test', 'Test'], "Auditors": ""},
    {"id": "0006", "Category": ['Test', 'Test', 'Test', 'Test', 'Medical'], "Auditors": ""},
]



@app.route('/', methods=['GET', 'POST'])
def newWorld():
    if request.method == 'GET':
        return render_template('main2.html', globallist=globallist, audits=audit)

    else:
        result = request.form.to_dict(flat=False)
        print(result)
        return result


if __name__ == '__main__':
    app.run(debug=True)
