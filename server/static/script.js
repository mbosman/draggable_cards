var list = [];

function onDragStart(event) {
  event.dataTransfer.setData("text/plain", event.target.id);
}
function onDragOver(event) {
  event.preventDefault();
}

function onDrop(event) {
  const id = event.dataTransfer.getData("text");
  console.log(id);
  const draggableElement = document.getElementById(id).cloneNode(true);
  var opt = document.createElement("option");
  var data = draggableElement.getElementsByTagName("h4")[0].textContent;
  if (!list.includes(data)) {
    opt.value = data;
    opt.selected = true;
    opt.innerHTML = data;
    console.log(opt);
    //   opt.appendChild(opt);
    list.push(data);
    const dropzone = event.target;
    //   console.log(dropzone);
    dropzone.appendChild(opt);
  } else {
    console.log("Already exists");
  }
}

function printmylist() {
  console.log(list);
}
