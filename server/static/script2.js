function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  var it = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(it));

  // Add functions to add data to the table
  var temp = document.getElementById("final").children;
  var children = [];
  for (i = 0; i < temp.length; i++) {
    console.log(temp[i].id);
    children.unshift(temp[i].id); // list of user names
    console.log(data); // all audits
    if (users[i].name == temp[i].id) {
      console.log("USER", users[i].name);
      users[i].skill.unshift(users[i].name);
    }
  }
  console.log("Children", children);
  generateTable(children);
}
$(document).ready(function () {
  total = document.getElementsByClassName("myCard").length;
});

function Sub() {
  var t = document.getElementById("final").children;
  for (let i = 0; i < t.length; i++) {
    var n = document.createElement("option");
    n.selected = true;
    var text = document.createTextNode(t[i].id);
    n.appendChild(text);

    document.getElementById("people").appendChild(n);
  }
  document.getElementById("myForm").submit();
}
function makeTable(event) {
  let table = document.getElementById("mytable");
  parent = table.parentNode;
  newtable = document.createElement("table");
  newtable.setAttribute("id", "mytable");
  newtable.setAttribute("class", "table table-bordered");
  categories = [];
  for (i = 0; i < data.length; i++) {
    // console.log(data[i].id);
    if (event.value == data[i].id) {
      categories = data[i].Category;
      categories.unshift("User");
    }
  }
  generateTableHead(newtable, categories);
  parent.insertBefore(newtable, table);
  parent.removeChild(table);

  var temp = document.getElementById("final").children;
  var children = [];
  for (i = 0; i < temp.length; i++) {
    console.log(temp[i].id);
    children.unshift(temp[i].id); // list of user names
    console.log(data); // all audits
    if (users[i].name == temp[i].id) {
      console.log("USER", users[i].name);
      users[i].skill.unshift(users[i].name);
    }
  }
  console.log("Children", children);
  generateTable(children);
}
function generateTableHead(table, d) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of d) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}
function generateTable(us) {
  // d for data
  let table = document.getElementById("mytable");
  if (table.getElementsByTagName("tbody")[0]) {
    console.log("We have a body");
    document.getElementsByTagName("tbody")[0].remove();
  }
  headoftable = [];
  var tbody = document.createElement("tbody");
  headoftable = table.children[0]
    .getElementsByTagName("tr")[0]
    .innerText.split("\t");
  console.log("Head of table", headoftable);
  headoftable.shift();
  console.log("Head of table", headoftable);

  console.log(["us", us]);
  for (let u of us) {
    for (r = 0; r < users.length; r++) {
      console.log("Users vs u", users[r].name, u);
      if (users[r].name == u) {
        myuser = users[r];
      }
    }
    let row = tbody.insertRow();
    row.insertCell().appendChild(document.createTextNode(myuser.name));

    // For every element in the header:
    for (let h of headoftable) {
      // loop through all the skills
      var found = false;
      for (let s of myuser.skill) {
        if (h == s) {
          console.log("same skill");
          let cell = row.insertCell();
          let text = document.createTextNode("");
          cell.style = "background-color: aqua";
          cell.appendChild(text);
          found = true;
          break;
        }
      }
      if (!found) {
        let cell = row.insertCell();
        cell.appendChild(document.createTextNode(""));
      }
    }
  }
  table.appendChild(tbody);
}
